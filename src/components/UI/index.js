import AppBanner from "./AppBanner.vue";
import AppWarningBanner from "./AppWarningBanner.vue";
import AppButton from "./AppButton.vue";
import AppInput from "./AppInput.vue";
import AppInfoText from "./AppInfoText.vue";

export default [
    AppBanner,
    AppButton,
    AppInput,
    AppWarningBanner,
    AppInfoText,
]